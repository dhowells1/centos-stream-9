
	List of RHEL maintainers and how to submit kernel changes

	OPTIONAL CC: the maintainers and mailing lists that are generated
	by redhat/scripts/rh_get_maintainer.pl.	 The results returned by the
	script will be best if you have git installed and are making
	your changes in a branch derived from the latest RHEL git tree.

Descriptions of section entries:

	M: Maintainer of the subsystem (Name and email)
	R: Reviewer of the subsystem (Name and email)
	L: Mailing list that is relevant to this area
	W: Web-page with status/info
	T: SCM tree type and location.	Type is one of: git, hg, quilt, stgit.
	S: Status, one of the following:
	   Supported:	This feature is supported.
	   Provided:	This feature is provided for a supported feature.
	   Internal:	This feature is only provided for internal use.
	F: Files and directories with wildcard patterns.
	   A trailing slash includes all files and subdirectory files.
	   F:	drivers/net/	all files in and below drivers/net
	   F:	drivers/net/*	all files in drivers/net, but not below
	   F:	*/net/*		all files in "any top level directory"/net
	   One pattern per line.  Multiple F: lines acceptable.
	X: Files and directories that are NOT maintained, same rules as F:
	   Files exclusions are tested before file matches.
	   Can be useful for excluding a specific subdirectory, for instance:
	   F:	net/
	   X:	net/ipv6/
	   matches all files in and below net excluding net/ipv6/
	N: Files and directories *Regex* patterns.
	   N:	[^a-z]tegra	all files whose path contains tegra
				(not including files like integrator)
	   One pattern per line.  Multiple N: lines acceptable.
	   scripts/get_maintainer.pl has different behavior for files that
	   match F: pattern and matches of N: patterns.  By default,
	   get_maintainer will not look at git log history when an F: pattern
	   match occurs.  When an N: match occurs, git log history is used
	   to also notify the people that have git commit signatures.
	K: *Content regex* (perl extended) pattern match in a patch or file.
	   For instance:
	   K: of_get_profile
	      matches patches or files that contain "of_get_profile"
	   K: \b(printk|pr_(info|err))\b
	      matches patches or files that contain one or more of the words
	      printk, pr_info or pr_err
	   One regex pattern per line.	Multiple K: lines acceptable.
	P: Person (obsolete)

Note: For the hard of thinking, this list is meant to remain in alphabetical
order. If you could add yourselves to it in alphabetical order that would be
so much easier [Ed]

Red Hat Maintainers List (try to look for most precise areas first)

		-----------------------------------
{% for subsystem in subsystems %}
{% if subsystem.status != 'Disabled' and subsystem.status != 'Unassigned' %}

{{ subsystem.subsystem }}
{% for maintainer in subsystem.maintainers %}
M:	{{ maintainer.name }} <{{maintainer.email}}>
{% endfor %}
{% for reviewer in subsystem.reviewers %}
R:	{{ reviewer.name }} <{{reviewer.email}}>
{% endfor %}
{% if subsystem.mailingList %}
L:	{{ subsystem.mailingList }}
{% endif %}
{% if subsystem.status %}
S:	{{ subsystem.status }}
{% endif %}
{% for path in subsystem.paths.includes %}
F:	{{ path }}
{% endfor %}
{% if subsystem.paths.includeRegexes %}
{% for path in subsystem.paths.includeRegexes %}
N:	{{ path }}
{% endfor %}
{% endif %}
{% if subsystem.paths.excludes %}
{% for path in subsystem.paths.excludes %}
X:	{{ path }}
{% endfor %}
{% endif %}
{% if subsystem.labels.emailLabel %}
I:	{{ subsystem.labels.emailLabel }}
{% endif %}
{% if subsystem.scm %}
T:	{{ subsystem.scm }}
{% endif %}
{% endif %}
{% endfor %}
